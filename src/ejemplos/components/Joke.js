import React from "react"

function Joke(props){
    return (
        <div>
            <h2 style={{display: props.question ?? "none"}}>Question: {props.question}</h2>
            <span style={{color: props.question ?? "gray"}}>Answer: {props.punchLine}</span>
        </div>
    )
}

export default Joke