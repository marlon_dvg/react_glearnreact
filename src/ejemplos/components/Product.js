import React from "react"

function Product(props) {
    return (
        <div className="product-container">
            <h2>{props.product.name}</h2>
            <p className="product-price">{props.product.price
                .toLocaleString("en-US", { style: "currency", currency: "USD" })}
            </p>
            <span className="product-description">{props.product.description}</span>
        </div>
    )
}

export default Product