import React from "react"

//UI component
function Form(props) {
    return (
        <main>
            <form onSubmit={props.handleSubmit}>
                <input
                    type="text"
                    name="firstName"
                    value={props.firstName}
                    placeholder="First name"
                    onChange={props.handleChange} />
                <input
                    type="text"
                    name="lastName"
                    value={props.lastName}
                    placeholder="Last name"
                    onChange={props.handleChange} />
                <br /><br />
                <input
                    type="number"
                    name="age"
                    value={props.age}
                    placeholder="Age"
                    onChange={props.handleChange} />
                <br /><br />
                <label>Male</label>
                <input
                    type="radio"
                    name="gender"
                    value="male"
                    checked={props.gender === "male"}
                    onChange={props.handleChange} />
                <label>Female</label>
                <input
                    type="radio"
                    name="gender"
                    value="female"
                    checked={props.gender === "female"}
                    onChange={props.handleChange} />
                <br /><br />
                <select
                    name="location"
                    value={props.location}
                    onChange={props.handleChange} >
                    <option value="">--Please choose location--</option>
                    <option value="france">France</option>
                    <option value="turkey">Turkey</option>
                    <option value="canada">Canada</option>
                </select>
                <br /><br />
                <label>Food restrictions:</label>
                <br />
                <input
                    type="checkbox"
                    name="isVegetarian"
                    checked={props.isVegetarian}
                    onChange={props.handleChange} />
                <label>Vegetarian</label>
                <br />
                <input
                    type="checkbox"
                    name="isKosher"
                    checked={props.isKosher}
                    onChange={props.handleChange} />
                <label>Kosher</label>
                <br />
                <input
                    type="checkbox"
                    name="isLactose"
                    checked={props.isLactose}
                    onChange={props.handleChange} />
                <label>Lactose</label>
                <br /><br />
                <button>Submit</button>
            </form>

            <h2>Name: {props.firstName} {props.lastName}</h2>
            <p>Age: {props.age}</p>
            <p>Gender: {props.gender}</p>
            <p>Location: {props.location}</p>
            <p>Is vegetarian: {props.isVegetarian ? "Yes" : "No"}</p>
            <p>Is kosher: {props.isKosher ? "Yes" : "No"}</p>
            <p>Is lactose: {props.isLactose ? "Yes" : "No"}</p>
        </main>
    )
}

export default Form