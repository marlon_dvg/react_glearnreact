import React, { Component } from "react"
import FormComponent from "./FormComponent"

//Bussiness component - Lógica de negocio
class Form extends Component {

    constructor() {
        super()
        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            location: "",
            isVegetarian: false,
            isKosher: false,
            isLactose: false,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        const { name, value, type, checked } = event.target

        type === "checkbox" ? this.setState({ [name]: checked })
            : this.setState({ [name]: value })
    }

    handleSubmit() {
        alert(
            "First name: " + this.state.firstName +
            "\nLast name: " + this.state.lastName +
            "\nAge: " + this.state.age +
            "\nLocation: " + this.state.location +
            "\nGender: " + this.state.gender +
            "\nFoor restrictions: " + (this.state.isVegetarian ? "Vegetarian" : "") + (this.state.isKosher ? " Kosher" : "")
            + (this.state.isLactose ? " Lactose" : "")
        )
    }

    render() {
        return (
            <FormComponent
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                {...this.state} /> //We pass all state props with the spread operator (...)
        )
    }
}

export default Form